# Xilinx Spartan Elbert V2

# Installation on Windows 10
(updated on 01.03.2020)
1. Open link: [Xilinx Design Tools Archive](https://www.xilinx.com/support/download/index.html/content/xilinx/en/downloadNav/vivado-design-tools/archive-ise.html)
2. Look for ISE Design Suite and download "Full Installer for Windows 7/XP/Server": [Xilinx ISE 14.7 Download](https://www.xilinx.com/member/forms/download/xef.html?filename=Xilinx_ISE_DS_Win_14.7_1015_1.tar)
3. During install select "ISE WebPack", later on I suggest default values until Finish button.
4. Manage Xilinx Licenses should start (if not - open it).<br>
a) In 'Acquire a license' check "Get Free Vivado/ISE WebPack License' -> Connect Now.<br>
b) Register on website and under "Create New Licenses" check "ISE WebPack License" -> Generate Node-Locked License.<br>
c) Download .lic file from your e-mail and load it in "Manage Xilinx Licenses" under "Manage Licenses" -> Load License.<br>
5. MOST IMPORTANT to get ISE work properly on Win10:<br>
a) Go to dir: *C:\Xilinx\14.7\ISE_DS\ISE\lib\nt64*<br>
b) RENAME file **libPortability.dll** to **libPortability.dll.orig**<br>
c) COPY file **libPortabilityNOSH.dll**<br>
d) Paste the copy of **libPortabilityNOSH.dll** to current directory and RENAME it to **libPortability.dll**<br>
e) Paste it (**libPortabilityNOSH.dll**) also to dir: *C:\Xilinx\14.7\ISE_DS\common\lib\nt64*<br>
f) Same as before - RENAME file **libPortability.dll** to **libPortability.dll.orig**<br>
g) Same as before - RENAME file **libPortabilityNOSH.dll** to **libPortability.dll**<br>

# Utils consists:
* Numato driver for Elbert V2
* ElbertV2Config -> Configuration loader
* ElbertV2 schematic
* ElbertV2 constraints file